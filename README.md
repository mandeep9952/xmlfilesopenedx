# README #

Export a Course

	To export your course, follow these steps.

	From the Tools menu, select Export.
	Select Export Course Content.
	When the export completes you can then access the .tar.gz file on your computer.

Import a Course

	To import a course, follow these steps.

	From the Tools menu of a course, select Import.
	Select Choose a File to Import.
	Locate the file that you want, and then Select Open.
	Select Replace my course with the one above.